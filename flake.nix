{
  description = "Device-specific system configuration for my nix systems";
  outputs = { z, ... }: abort
    ''
      You tried to use ruro-nix/devz without specifying the branch.
      Each branch in this repo contains overrides for specific devices.
    '';
}
